From: =?utf-8?q?David_Pr=C3=A9vot?= <taffit@debian.org>
Date: Tue, 18 Feb 2025 01:40:18 +0100
Subject: Modernize PHPUnit syntax

---
 tests/Persistence/ManagerRegistryTest.php          |  6 +++---
 .../Mapping/AbstractClassMetadataFactoryTest.php   |  3 +++
 .../Mapping/ColocatedMappingDriverTest.php         |  3 ++-
 tests/Persistence/Mapping/DriverChainTest.php      | 12 ++++++------
 tests/Persistence/Mapping/FileDriverTest.php       | 22 +++++++++++-----------
 .../Persistence/Mapping/SymfonyFileLocatorTest.php |  7 +++----
 .../Persistence/RuntimeReflectionPropertyTest.php  | 12 +++++-------
 7 files changed, 33 insertions(+), 32 deletions(-)

diff --git a/tests/Persistence/ManagerRegistryTest.php b/tests/Persistence/ManagerRegistryTest.php
index b01a48b..a066c50 100644
--- a/tests/Persistence/ManagerRegistryTest.php
+++ b/tests/Persistence/ManagerRegistryTest.php
@@ -85,7 +85,7 @@ class ManagerRegistryTest extends DoctrineTestCase
             ->expects(self::once())
             ->method('getRepository')
             ->with(self::equalTo(TestObject::class))
-            ->will(self::returnValue($repository));
+            ->willReturn($repository);
 
         self::assertSame($repository, $this->mr->getRepository(TestObject::class));
     }
@@ -116,7 +116,7 @@ class ManagerRegistryTest extends DoctrineTestCase
             ->expects(self::once())
             ->method('getRepository')
             ->with(self::equalTo(TestObject::class))
-            ->will(self::returnValue($repository));
+            ->willReturn($repository);
 
         self::assertSame($repository, $this->mr->getRepository(TestObject::class, 'other'));
     }
@@ -147,7 +147,7 @@ class ManagerRegistryTest extends DoctrineTestCase
             ->expects(self::once())
             ->method('getRepository')
             ->with(self::equalTo(OtherTestObject::class))
-            ->will(self::returnValue($repository));
+            ->willReturn($repository);
 
         self::assertSame($repository, $this->mr->getRepository(OtherTestObject::class));
     }
diff --git a/tests/Persistence/Mapping/AbstractClassMetadataFactoryTest.php b/tests/Persistence/Mapping/AbstractClassMetadataFactoryTest.php
index 0393447..b31584e 100644
--- a/tests/Persistence/Mapping/AbstractClassMetadataFactoryTest.php
+++ b/tests/Persistence/Mapping/AbstractClassMetadataFactoryTest.php
@@ -9,9 +9,12 @@ use Doctrine\Persistence\Mapping\ClassMetadata;
 use Doctrine\Persistence\Mapping\Driver\MappingDriver;
 use Doctrine\Persistence\Mapping\MappingException;
 use Doctrine\Tests\DoctrineTestCase;
+use PHPUnit\Framework\Attributes\RequiresPhpunit;
 
+#[RequiresPhpunit('< 12')]
 final class AbstractClassMetadataFactoryTest extends DoctrineTestCase
 {
+    #[RequiresPhpunit('< 10')]
     public function testItSkipsTransientClasses(): void
     {
         $cmf = $this->getMockForAbstractClass(AbstractClassMetadataFactory::class);
diff --git a/tests/Persistence/Mapping/ColocatedMappingDriverTest.php b/tests/Persistence/Mapping/ColocatedMappingDriverTest.php
index cbb4fea..f16feb2 100644
--- a/tests/Persistence/Mapping/ColocatedMappingDriverTest.php
+++ b/tests/Persistence/Mapping/ColocatedMappingDriverTest.php
@@ -9,6 +9,7 @@ use Doctrine\Persistence\Mapping\Driver\ColocatedMappingDriver;
 use Doctrine\Persistence\Mapping\Driver\MappingDriver;
 use Doctrine\TestClass;
 use Generator;
+use PHPUnit\Framework\Attributes\DataProvider;
 use PHPUnit\Framework\TestCase;
 
 use function array_values;
@@ -54,7 +55,7 @@ class ColocatedMappingDriverTest extends TestCase
         self::assertSame('.php1', $driver->getFileExtension());
     }
 
-    /** @dataProvider pathProvider */
+    #[DataProvider('pathProvider')]
     public function testGetAllClassNames(string $path): void
     {
         $driver = $this->createDriver($path);
diff --git a/tests/Persistence/Mapping/DriverChainTest.php b/tests/Persistence/Mapping/DriverChainTest.php
index 3199bc7..37f5b1f 100644
--- a/tests/Persistence/Mapping/DriverChainTest.php
+++ b/tests/Persistence/Mapping/DriverChainTest.php
@@ -63,12 +63,12 @@ class DriverChainTest extends DoctrineTestCase
         $driver1 = $this->createMock(MappingDriver::class);
         $driver1->expects(self::once())
                 ->method('getAllClassNames')
-                ->will(self::returnValue(['Doctrine\Tests\Models\Company\Foo']));
+                ->willReturn(['Doctrine\Tests\Models\Company\Foo']);
 
         $driver2 = $this->createMock(MappingDriver::class);
         $driver2->expects(self::once())
                 ->method('getAllClassNames')
-                ->will(self::returnValue(['Doctrine\Tests\ORM\Mapping\Bar', 'Doctrine\Tests\ORM\Mapping\Baz', 'FooBarBaz']));
+                ->willReturn(['Doctrine\Tests\ORM\Mapping\Bar', 'Doctrine\Tests\ORM\Mapping\Baz', 'FooBarBaz']);
 
         $chain->addDriver($driver1, 'Doctrine\Tests\Models\Company');
         $chain->addDriver($driver2, 'Doctrine\Tests\ORM\Mapping');
@@ -104,14 +104,14 @@ class DriverChainTest extends DoctrineTestCase
         $companyDriver->expects(self::once())
             ->method('isTransient')
             ->with(self::equalTo($managerClassName))
-            ->will(self::returnValue(false));
+            ->willReturn(false);
 
         $defaultDriver->expects(self::never())
             ->method('loadMetadataForClass');
         $defaultDriver->expects(self::once())
             ->method('isTransient')
             ->with(self::equalTo($entityClassName))
-            ->will(self::returnValue(true));
+            ->willReturn(true);
 
         self::assertNull($chain->getDefaultDriver());
 
@@ -134,11 +134,11 @@ class DriverChainTest extends DoctrineTestCase
 
         $companyDriver->expects(self::once())
             ->method('getAllClassNames')
-            ->will(self::returnValue(['Doctrine\Tests\Models\Company\Foo']));
+            ->willReturn(['Doctrine\Tests\Models\Company\Foo']);
 
         $defaultDriver->expects(self::once())
             ->method('getAllClassNames')
-            ->will(self::returnValue(['Other\Class']));
+            ->willReturn(['Other\Class']);
 
         $chain->setDefaultDriver($defaultDriver);
         $chain->addDriver($companyDriver, 'Doctrine\Tests\Models\Company');
diff --git a/tests/Persistence/Mapping/FileDriverTest.php b/tests/Persistence/Mapping/FileDriverTest.php
index 8954fb6..1113b1e 100644
--- a/tests/Persistence/Mapping/FileDriverTest.php
+++ b/tests/Persistence/Mapping/FileDriverTest.php
@@ -47,7 +47,7 @@ class FileDriverTest extends DoctrineTestCase
         $locator->expects(self::once())
                 ->method('findMappingFile')
                 ->with(self::equalTo(stdClass::class))
-                ->will(self::returnValue(__DIR__ . '/_files/stdClass.yml'));
+                ->willReturn(__DIR__ . '/_files/stdClass.yml');
 
         $driver = $this->createTestFileDriver($locator);
 
@@ -62,7 +62,7 @@ class FileDriverTest extends DoctrineTestCase
         $locator->expects(self::once())
             ->method('findMappingFile')
             ->with(self::equalTo(stdClass::class))
-            ->will(self::returnValue(__DIR__ . '/_files/stdClass.yml'));
+            ->willReturn(__DIR__ . '/_files/stdClass.yml');
 
         $driver = $this->createTestFileDriver($locator);
 
@@ -76,10 +76,10 @@ class FileDriverTest extends DoctrineTestCase
     public function testGetAllClassNamesGlobalBasename(): void
     {
         $locator = $this->newLocator();
-        $locator->expects(self::any())->method('getAllClassNames')->with('global')->will(self::returnValue([
+        $locator->expects(self::any())->method('getAllClassNames')->with('global')->willReturn([
             GlobalClass::class,
             AnotherGlobalClass::class,
-        ]));
+        ]);
 
         $driver = $this->createTestFileDriver($locator);
         $driver->setGlobalBasename('global');
@@ -95,7 +95,7 @@ class FileDriverTest extends DoctrineTestCase
         $locator->expects(self::any())
                 ->method('getAllClassNames')
                 ->with(self::equalTo(null))
-                ->will(self::returnValue([stdClass::class]));
+                ->willReturn([stdClass::class]);
         $driver = new TestFileDriver($locator);
 
         $classNames = $driver->getAllClassNames();
@@ -109,7 +109,7 @@ class FileDriverTest extends DoctrineTestCase
         $locator->expects(self::any())
                 ->method('getAllClassNames')
                 ->with(self::equalTo('global'))
-                ->will(self::returnValue([stdClass::class]));
+                ->willReturn([stdClass::class]);
         $driver = new TestFileDriver($locator);
         $driver->setGlobalBasename('global');
 
@@ -128,7 +128,7 @@ class FileDriverTest extends DoctrineTestCase
         $locator->expects(self::once())
                 ->method('findMappingFile')
                 ->with(self::equalTo(stdClass::class))
-                ->will(self::returnValue(__DIR__ . '/_files/stdClass.yml'));
+                ->willReturn(__DIR__ . '/_files/stdClass.yml');
         $driver = new TestFileDriver($locator);
         $driver->setGlobalBasename('global');
 
@@ -144,7 +144,7 @@ class FileDriverTest extends DoctrineTestCase
         $locator->expects(self::once())
                 ->method('fileExists')
                 ->with(self::equalTo(stdClass::class))
-                ->will(self::returnValue(true));
+                ->willReturn(true);
 
         $driver = $this->createTestFileDriver($locator);
         $driver->setGlobalBasename('global');
@@ -160,7 +160,7 @@ class FileDriverTest extends DoctrineTestCase
         $locator->expects(self::once())
                 ->method('fileExists')
                 ->with(self::equalTo(NotLoadedClass::class))
-                ->will(self::returnValue(false));
+                ->willReturn(false);
 
         $driver = $this->createTestFileDriver($locator);
 
@@ -178,8 +178,8 @@ class FileDriverTest extends DoctrineTestCase
     private function newLocator(): MockObject
     {
         $locator = $this->createMock(FileLocator::class);
-        $locator->expects(self::any())->method('getFileExtension')->will(self::returnValue('.yml'));
-        $locator->expects(self::any())->method('getPaths')->will(self::returnValue([__DIR__ . '/_files']));
+        $locator->expects(self::any())->method('getFileExtension')->willReturn('.yml');
+        $locator->expects(self::any())->method('getPaths')->willReturn([__DIR__ . '/_files']);
 
         return $locator;
     }
diff --git a/tests/Persistence/Mapping/SymfonyFileLocatorTest.php b/tests/Persistence/Mapping/SymfonyFileLocatorTest.php
index 7d5e42d..632047b 100644
--- a/tests/Persistence/Mapping/SymfonyFileLocatorTest.php
+++ b/tests/Persistence/Mapping/SymfonyFileLocatorTest.php
@@ -8,6 +8,7 @@ use Doctrine\Persistence\Mapping\Driver\SymfonyFileLocator;
 use Doctrine\Persistence\Mapping\MappingException;
 use Doctrine\Tests\DoctrineTestCase;
 use InvalidArgumentException;
+use PHPUnit\Framework\Attributes\DataProvider;
 
 use function realpath;
 use function sort;
@@ -106,9 +107,8 @@ class SymfonyFileLocatorTest extends DoctrineTestCase
      * @param string $dir       Path to load mapping data from
      *
      * @throws MappingException
-     *
-     * @dataProvider customNamespaceSeparatorProvider
      */
+    #[DataProvider('customNamespaceSeparatorProvider')]
     public function testGetClassNamesWithCustomNsSeparator(string $separator, string $dir): void
     {
         $path   = __DIR__ . $dir;
@@ -152,9 +152,8 @@ class SymfonyFileLocatorTest extends DoctrineTestCase
      * @param string[] $files     Files to lookup classnames
      *
      * @throws MappingException
-     *
-     * @dataProvider customNamespaceLookupQueryProvider
      */
+    #[DataProvider('customNamespaceLookupQueryProvider')]
     public function testFindMappingFileWithCustomNsSeparator(string $separator, string $dir, array $files): void
     {
         $path   = __DIR__ . $dir;
diff --git a/tests/Persistence/RuntimeReflectionPropertyTest.php b/tests/Persistence/RuntimeReflectionPropertyTest.php
index 930cace..0af83ee 100644
--- a/tests/Persistence/RuntimeReflectionPropertyTest.php
+++ b/tests/Persistence/RuntimeReflectionPropertyTest.php
@@ -7,6 +7,7 @@ namespace Doctrine\Tests\Persistence;
 use Closure;
 use Doctrine\Persistence\Proxy;
 use Doctrine\Persistence\Reflection\RuntimeReflectionProperty;
+use PHPUnit\Framework\Attributes\TestWith;
 use PHPUnit\Framework\TestCase;
 
 class DummyMock
@@ -22,10 +23,8 @@ class DummyMock
 
 class RuntimeReflectionPropertyTest extends TestCase
 {
-    /**
-     * @testWith ["test", "testValue"]
-     *           ["privateTest", "privateTestValue"]
-     */
+    #[testWith(["test", "testValue"])]
+    #[testWith(["privateTest", "privateTestValue"])]
     public function testGetSetValue(string $name, string $value): void
     {
         $object = new RuntimeReflectionPropertyTestClass();
@@ -42,10 +41,9 @@ class RuntimeReflectionPropertyTest extends TestCase
 
     /**
      * @param class-string<RuntimeReflectionPropertyTestProxyMock> $proxyClass
-     *
-     * @testWith ["Doctrine\\Tests\\Persistence\\RuntimeReflectionPropertyTestProxyMock"]
-     *           ["\\Doctrine\\Tests\\Persistence\\RuntimeReflectionPropertyTestProxyMock"]
      */
+    #[testWith(["Doctrine\\Tests\\Persistence\\RuntimeReflectionPropertyTestProxyMock"])]
+    #[testWith(["\\Doctrine\\Tests\\Persistence\\RuntimeReflectionPropertyTestProxyMock"])]
     public function testGetValueOnProxyProperty(string $proxyClass): void
     {
         $getCheckMock = $this->createMock(DummyMock::class);
